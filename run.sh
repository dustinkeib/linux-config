#!/bin/bash

docker-compose rm -s -f -v \
  && docker-compose build \
  && docker-compose up -d \
  && docker ps \
  && docker-compose logs -f
