#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
apt update -qy
apt install -qy virtualenvwrapper git mlocate wget curl libxss-dev build-essential iputils-ping traceroute nmap dnsutils tzdata
ln -fs /usr/share/zoneinfo/America/Los_Angeles /etc/localtime
dpkg-reconfigure --frontend noninteractive tzdata
