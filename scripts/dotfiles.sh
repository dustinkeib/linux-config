#!/bin/bash
cd $HOME
git clone --bare https://dustinkeib@bitbucket.org/dustinkeib/cfg.git $HOME/.cfg
config() {
    /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@" >> $HOME/.bashrc
export -f config
mkdir -p .config-backup
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files."
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .config-backup/{}
fi;
config checkout
config config status.showUntrackedFiles no
config submodule update --init --recursive
sudo chsh -s $(grep /zsh$ /etc/shells | tail -1)
