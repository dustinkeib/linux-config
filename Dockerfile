FROM ubuntu
RUN apt update -qy && apt install -qy git sudo
RUN \
    groupadd -g 1000 dustin && useradd -u 1000 -g dustin -G sudo -m -s /bin/bash dustin && \
    sed -i /etc/sudoers -re 's/^%sudo.*/%sudo ALL=(ALL:ALL) NOPASSWD: ALL/g' && \
    sed -i /etc/sudoers -re 's/^root.*/root ALL=(ALL:ALL) NOPASSWD: ALL/g' && \
    sed -i /etc/sudoers -re 's/^#includedir.*/## **Removed the include directive** ##"/g' && \
    echo "dustin ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers && \
    echo "Customized the sudoers file for passwordless access to the dustin user!" && \
    echo "dustin user:";  su - dustin -c id
USER dustin
WORKDIR /home/dustin/scripts
COPY ./scripts/. /home/dustin/scripts/
RUN sudo chown -R dustin:dustin /home/dustin/scripts